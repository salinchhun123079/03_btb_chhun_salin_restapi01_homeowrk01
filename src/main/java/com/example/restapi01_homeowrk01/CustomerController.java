package com.example.restapi01_homeowrk01;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.util.ArrayList;

@RestController
public class CustomerController {
    ArrayList<Customer> customers = new ArrayList<>();
    ArrayList<Customer> customerRequests = new ArrayList<Customer>();

    //default value
    public CustomerController() {
        customers.add(new Customer(1, "Salin", "M", 22, "PP"));
        customers.add(new Customer(2, "Davin", "M", 20, "PV"));
    }

    //show all customer
    @GetMapping("/api/v1/customers")
    public ResponseEntity<?> getAllCustomer() {
        return ResponseEntity.ok(new CustomerResponse<ArrayList<Customer>>(
                LocalDateTime.now(),
                "ok",
                "You get all customer successfully",
                customers
        ));
    }

    //insert customer
    int autoId = 2;
    @PostMapping("/api/v1/customers")
    public ResponseEntity<CustomerResponse<Customer>> insertCustomer(@RequestBody CustomerRequest customerRequest) {
        autoId = autoId + 1;
        customers.add(new Customer(autoId, customerRequest.getName(), customerRequest.getGender(), customerRequest.getAge(), customerRequest.getAddress()));
        Customer customer = new Customer();
        for (Customer cus : customers) {
            if (cus.getId() == autoId) {
                customer = cus;
                break;
            }
        }
        return ResponseEntity.ok(new CustomerResponse<Customer>(
                LocalDateTime.now(),
                "ok",
                "You have created successfully",
                customers.get(autoId-1)
        ));
    }

    //search product byId
    @GetMapping("/api/v1/customers/{customerId}")
    public Customer getCustomerById(@PathVariable Integer customerId) {
        for (Customer cus : customers)
            if (cus.getId() == customerId) {
                return cus;
            }
        return null;
    }

    //search product byName
    @GetMapping("/api/v1/customers/search")
    public Customer findCustomerByName(@RequestParam String name) {
        for (Customer cus : customers)
            if (cus.getName().equals(name)) {
                return cus;
            }
        return null;
    }

    //delete customer
    @DeleteMapping("/api/v1/customer/{id}")
    public ResponseEntity<?> deleteCustomer(@PathVariable Integer id) {
        customers.remove(id - 1);
        return ResponseEntity.ok("You deleted successfully");

    }

    //update customer
    @PutMapping("/api/v1/customerId/{id}")
    public ResponseEntity <CustomerResponse<Customer>> UpdateCustomer(@PathVariable int id, @RequestBody CustomerRequest customerRequest) {
        Customer customer = new Customer();
        for (Customer cus : customers) {
            if (cus.getId() == id) {
                cus.setName(customerRequest.getName());
                cus.setGender(customerRequest.getGender());
                cus.setAge(customerRequest.getAge());
                cus.setAddress(customerRequest.getAddress());
                break;
            }
        }
        return ResponseEntity.ok(new CustomerResponse<Customer>(
                LocalDateTime.now(),
                "ok",
                "You have been updated successfully",
                customers.get(id-1)
        ));
    }
}
