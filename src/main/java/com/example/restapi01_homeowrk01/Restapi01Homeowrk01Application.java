package com.example.restapi01_homeowrk01;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Restapi01Homeowrk01Application {

    public static void main(String[] args) {

        SpringApplication.run(Restapi01Homeowrk01Application.class, args);

    }

}
