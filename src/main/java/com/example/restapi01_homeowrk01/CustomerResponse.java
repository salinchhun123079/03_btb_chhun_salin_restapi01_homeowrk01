package com.example.restapi01_homeowrk01;

import java.time.LocalDateTime;

public class CustomerResponse<T> {
    private LocalDateTime dateTime;
    private String status;
    private String message;
    private T customer;

    public CustomerResponse(LocalDateTime dateTime, String status, String message, T customer) {
        this.dateTime = dateTime;
        this.status = status;
        this.message = message;
        this.customer = customer;
    }

    public LocalDateTime getDateTime() {
        return dateTime;
    }

    public void setDateTime(LocalDateTime dateTime) {
        this.dateTime = dateTime;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public T getCustomer() {
        return customer;
    }

    public void setCustomer(T customer) {
        this.customer = customer;
    }
}
